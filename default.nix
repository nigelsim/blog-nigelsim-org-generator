{ nixpkgs ? import <nixpkgs> {} }:
let
  inherit (nixpkgs) pkgs;
  haskellPackages = nixpkgs.haskellPackages.override {
    overrides = self: super: {
      blog-nigelsim-org = self.callCabal2nix "blog-nigelsim-org" ./. {};
    };
  };
in
  haskellPackages.blog-nigelsim-org