--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Maybe (fromMaybe)
import           Data.Monoid (mappend, (<>))
import           Hakyll
import           System.FilePath ( (</>), (<.>)
                                 , splitExtension, splitFileName
                                 , takeDirectory )
import           Control.Monad         (liftM)
import Skylighting (addSyntaxDefinition, defaultSyntaxMap, parseSyntaxDefinition)
import qualified Data.Text as T
import Text.Pandoc (Pandoc, runPure)
import Text.Pandoc.Writers.HTML (writeRevealJs)
import Text.Pandoc.Shared (headerShift)
import Text.Pandoc.Options
  ( Extension(Ext_literate_haskell)
  , ReaderOptions(..)
  , WriterOptions(..)
  , disableExtension
  , enableExtension
  , HTMLSlideVariant(..)
  )
import Text.Pandoc.Templates (getDefaultTemplate)
--------------------------------------------------------------------------------

preambleForTag 
  :: String 
  -> [Item String] 
  -> Compiler (Maybe String)
preambleForTag tag preambles =
  let
    headMay [] = 
      Nothing
    headMay (x:_) = 
      Just x
    matchesTag i = 
      case capture "tag-preambles/*.md" i of
        Just [x] -> x == tag
        _ -> False
  in
    pure . 
    fmap itemBody . 
    headMay . 
    filter (matchesTag . itemIdentifier) $ 
    preambles


writePandocSlideWith :: WriterOptions -> Item Pandoc -> Item String
writePandocSlideWith wopt (Item itemi doc) =
    case runPure $ writeRevealJs wopt doc of
        Left err    -> error $ "writePandocSlidesWith: " ++ show err
        Right item' -> Item itemi $ T.unpack item'

pandocCompileSlides :: ReaderOptions -> WriterOptions -> Compiler (Item String)
pandocCompileSlides ropt wopt =
    cached "pandocCompileSlides" $
        writePandocSlideWith wopt <$>
        (traverse (return.id) =<< readPandocWith ropt =<< getResourceBody)

static :: Pattern -> Rules ()
static f = match f $ do
    route   idRoute
    compile copyFileCompiler

directory :: (Pattern -> Rules a) -> String -> Rules a
directory act f = act $ fromGlob $ f ++ "/**"

main = do
    f <- parseSyntaxDefinition "highlighting/groovy.xml"
    writerOpts <- case f of
      Left e -> fail e
      Right s -> return $ defaultHakyllWriterOptions
        { writerSyntaxMap = addSyntaxDefinition s defaultSyntaxMap
        }
    
    
    hakyll $ do

        mapM_ (directory static) ["assets", "images"]

        match "css/*" $ do
            route   idRoute
            compile compressCssCompiler

        match (fromList ["about.rst", "contact.markdown"]) $ do
            route   $ setExtension "html" `composeRoutes` appendIndex
            let siteCtx = dropIndexHtml "url" `mappend` defaultContext
            compile $ pandocCompiler
                >>= loadAndApplyTemplate "templates/default.html" siteCtx
                >>= relativizeUrls

        match "tag-preambles/*.md" $ do
            route $ setExtension "html"
            compile $ pandocCompiler

        tags <- buildTags "posts/*.md" (fromCapture "tags/*.html")
        tagsRules tags $ \tag pattern -> do
            let title = "Posts tagged \"" ++ tag ++ "\""
            route idRoute
            compile $ do
                posts <- recentFirst =<< loadAll pattern
                mPreamble <- preambleForTag tag =<< loadAll "tag-preambles/*.html"
                let 
                    preambleCtx = maybe mempty (constField "preamble") mPreamble
                    ctx = constField "title" title
                        `mappend` preambleCtx
                        `mappend` listField "posts" postCtx (return posts)
                        `mappend` defaultContext

                makeItem ""
                    >>= loadAndApplyTemplate "templates/tag.html" ctx
                    >>= loadAndApplyTemplate "templates/default.html" ctx
                    >>= relativizeUrls

        match "posts/*.md" $ do
            route $ setExtension "html" `composeRoutes`
                    dateFolders         `composeRoutes`
                    dropPostsPrefix     `composeRoutes`
                    appendIndex
            compile $ pandocCompilerWithTransform defaultHakyllReaderOptions writerOpts (headerShift 1)
                >>= loadAndApplyTemplate "templates/post.html"    (postCtxWithTags tags)
                >>= saveSnapshot "content"
                >>= loadAndApplyTemplate "templates/default.html" (postCtxWithTags tags)
                >>= relativizeUrls

        create ["archive.html"] $ do
            route appendIndex
            compile $ do
                posts <- recentFirst =<< loadAll "posts/*"
                let archiveCtx =
                        listField "posts" postCtx (return posts) `mappend`
                        constField "title" "Archives"            `mappend`
                        dropIndexHtml "url"                      `mappend`
                        defaultContext

                makeItem ""
                    >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                    >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                    >>= relativizeUrls


        match "index.html" $ do
            route idRoute
            compile $ do
                posts <- fmap (take 5) . recentFirst =<< loadAll "posts/*"
                let indexCtx =
                        listField "posts" postCtx (return posts) `mappend`
                        constField "title" "Home"                `mappend`
                        defaultContext

                getResourceBody
                    >>= applyAsTemplate indexCtx
                    >>= loadAndApplyTemplate "templates/default.html" indexCtx
                    >>= relativizeUrls

        match "templates/*" $ compile templateCompiler

        match "presentations/*" $ do
            route $ setExtension "html"
            compile $ pandocCompileSlides defaultHakyllReaderOptions defaultHakyllWriterOptions
                    >>= loadAndApplyTemplate "templates/presentation.html" defaultContext
                    >>= relativizeUrls

        create ["rss.xml"] $ do
            route idRoute
            compile $ do
                posts <- loadAllSnapshots "posts/*" "content"
                sorted <- recentFirst posts
                renderRss feedConfiguration feedCtx (take 10 sorted)

        create ["atom.xml"] $ do
            route idRoute
            compile $ do
                posts <- loadAllSnapshots "posts/*" "content"
                sorted <- recentFirst posts
                renderAtom feedConfiguration feedCtx sorted

        create ["sitemap.xml"] $ do
            route idRoute
            compile $ do
                -- load and sort the posts
                posts <- recentFirst =<< loadAll "posts/*"

                -- load individual pages from a list (globs DO NOT work here)
                singlePages <- loadAll (fromList ["about.rst", "contact.markdown"])

                                -- mappend the posts and singlePages together
                let pages = posts <> singlePages

                                -- create the `pages` field with the postCtxWithTags
                                -- and return the `pages` value for it
                    sitemapCtx = 
                        constField "root" (feedRoot feedConfiguration) <>
                        listField "pages" postCtx (return pages)

                -- make the item and apply our sitemap template
                makeItem ""
                    >>= loadAndApplyTemplate "templates/sitemap.xml" sitemapCtx


--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    constField "root" (feedRoot feedConfiguration) <>
    dateField "date" "%Y-%m-%d"      `mappend`
    dateField "dateLong" "%B %e, %Y" `mappend`
    dropIndexHtml "url"              `mappend`
    defaultContext

postCtxWithTags :: Tags -> Context String
postCtxWithTags tags = tagsField "tags" tags `mappend` postCtx

--------------------------------------------------------------------------------
dateFolders :: Routes
dateFolders =
    gsubRoute "/[0-9]{4}-[0-9]{2}-[0-9]{2}-" $ replaceAll "-" (const "/")


--------------------------------------------------------------------------------
appendIndex :: Routes
appendIndex = customRoute $
    (\(p, e) -> p </> "index" <.> e) . splitExtension . toFilePath


--------------------------------------------------------------------------------
dropPostsPrefix :: Routes
dropPostsPrefix = gsubRoute "posts/" $ const ""


--------------------------------------------------------------------------------
prependCategory :: Routes
prependCategory = metadataRoute $ \md -> customRoute $
    let mbCategory = lookupString "category" md
        category = fromMaybe (error "Posts: Post without category") mbCategory
    in  (category </>) . toFilePath


--------------------------------------------------------------------------------
dropIndexHtml :: String -> Context a
dropIndexHtml key = mapContext transform (urlField key) where
    transform url = case splitFileName url of
                        (p, "index.html") -> takeDirectory p
                        _                 -> url

feedCtx :: Context String
feedCtx =
    bodyField "description" `mappend`
    postCtx

feedConfiguration :: FeedConfiguration
feedConfiguration = FeedConfiguration
    { feedTitle       = "Nigel Sim : All posts"
    , feedDescription = "My personal blog on software, technology, and life."
    , feedAuthorName  = "Nigel Sim"
    , feedAuthorEmail = "nigel.sim@gmail.com"
    , feedRoot        = "https://blog.nigelsim.org"
    }
